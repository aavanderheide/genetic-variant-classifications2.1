Experiment CVC
Analysing:  Percent_correct
Confidence: 0.05 (two tailed)
Dataset                   (1) meta.Cla | (2) meta. (3) meta. (4) meta.
----------------------------------------------------------------------
ProjectDatasetNon-Conflic(100)   59.32 | (2) 58.68 (3) 67.23 (4) 54.78  
----------------------------------------------------------------------



Key:
(1) meta.ClassificationViaClustering random
(2) meta.ClassificationViaClustering kmeans++
(3) meta.ClassificationViaClustering Canopy
(4) meta.ClassificationViaClustering Farthest First
